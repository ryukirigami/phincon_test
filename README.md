# INSTALL 
```sh
$ yarn install
$ cd server && yarn install
# or
$ npm install
$ cd server && npm install
```

# RUN
```sh
$ yarn start
# or
$ npm start
```

# BUILD
```sh
$ yarn build
# or
$ npm build
```
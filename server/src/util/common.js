const isPrime = (num) => {
  for (let i = 2; i < num; i++) { if (num % i === 0) return false; }
  return true;
};

module.exports = {
  claimCatchPokemonChange: () => {
    const getChance = Math.round(Math.random() * 100);
    if (getChance < 50) {
      throw new Error(`You got ${getChance}% change to capture this pokemon, Pokemon has run away!`);
    }
    return getChance;
  },

  claimRelesePokemonChance: () => {
    const releaseChance = Math.round(Math.random() * 100);
    if (!isPrime(releaseChance)) {
      throw new Error(`You got number ${releaseChance}, Pokemon has failed to be released.`);
    }
    return releaseChance;
  },

  getFibonacciSeq: (currentSeqNum, lastSeqNum) => {
    if (currentSeqNum === undefined) return 0;
    if (currentSeqNum === 0) return 1;

    return currentSeqNum + lastSeqNum;
  },
};

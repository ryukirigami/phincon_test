const { v4 } = require('uuid');
const { claimCatchPokemonChange, getFibonacciSeq, claimRelesePokemonChance } = require('../../util/common');
const MyPokemon = require('../../store/MyPokemon');
const Pokemon = require('../../store/Pokemon');

const getAllPokemon = async () => {
  try {
    return { pokemonList: MyPokemon.getPokemons() };
  } catch (error) {
    return { errors: error.message };
  }
};

const catchPokemon = async (req) => {
  const { body } = req;

  try {
    const captureChance = claimCatchPokemonChange();
    const pokemon = new Pokemon(v4(), body.pokemonId, body.pokemonName);
    MyPokemon.addPokemon(pokemon);

    return { message: `You got ${captureChance}% to capture this pokemon, Pokemon ha been captured!`, pokemon };
  } catch (error) {
    return { errors: error.message };
  }
};

const renamePokemon = async (req) => {
  const { body, params } = req;

  try {
    // check is the pokemon exist
    const pokemon = MyPokemon.getPokemonByUUID(params.uuid);
    if (pokemon.length < 1) throw new Error('Pokemon does not found.');

    const { currentSeqNum, lastSeqNum } = pokemon[0];

    const nextSeqNum = getFibonacciSeq(currentSeqNum, lastSeqNum);
    pokemon[0].lastSeqNum = currentSeqNum;
    pokemon[0].currentSeqNum = nextSeqNum;
    pokemon[0].nickname = `${body.nickname}-${nextSeqNum}`;

    MyPokemon.updatePokemon(pokemon.uuid, pokemon);

    return { message: `Pokemon name has changed to ${pokemon[0].nickname}!` };
  } catch (error) {
    return { errors: error.message };
  }
};

const releasePokemon = async (req) => {
  const { params } = req;

  try {
    // check is the pokemon exist
    if (MyPokemon.getPokemonIndexByUUID(params.uuid) === -1) throw new Error('Pokemon does not found.');

    const releaseChance = claimRelesePokemonChance();
    MyPokemon.removePokemon(params.uuid);

    return { message: `You got number ${releaseChance}, It's Prime!!, Pokemon has been released!` };
  } catch (error) {
    return { errors: error.message };
  }
};

module.exports = {
  getAllPokemon,
  catchPokemon,
  renamePokemon,
  releasePokemon,
};

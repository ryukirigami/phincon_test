const express = require('express');
const pokemonController = require('./pokemonController');
const validation = require('../../middleware/validation');
const { catchPokemonSchema, renamePokemonSchema } = require('./pokemonSchema');

const route = express.Router();

route.get('/', pokemonController.getAll);
route.post('/catch', validation(catchPokemonSchema), pokemonController.catchPokemon);
route.put('/rename/:uuid', validation(renamePokemonSchema), pokemonController.renamePokemon);
route.delete('/release/:uuid', pokemonController.releasePokemon);

module.exports = route;

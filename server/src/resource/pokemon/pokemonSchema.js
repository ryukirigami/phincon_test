const { number } = require('yup');
const { object, string } = require('yup');

const catchPokemonSchema = object({
  pokemonId: number().required(),
  pokemonName: string().required(),
});

const renamePokemonSchema = object({
  nickname: string().required(),
});

module.exports = {
  catchPokemonSchema,
  renamePokemonSchema,
};

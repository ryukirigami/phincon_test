const pokemonService = require('./pokemonService');
const ApiError = require('../../dto/ApiError');
const Response = require('../../dto/Response');

const pokemonController = {

  getAll: (req, res, next) => pokemonService.getAllPokemon()
    .then((result) => {
      if (result.errors) return next(ApiError.badRequest(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => next(ApiError.serverError(err.message))),

  catchPokemon: (req, res, next) => pokemonService.catchPokemon(req)
    .then((result) => {
      if (result.errors) return next(ApiError.badRequest(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => next(ApiError.serverError(err.message))),

  renamePokemon: (req, res, next) => pokemonService.renamePokemon(req)
    .then((result) => {
      if (result.errors) return next(ApiError.badRequest(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => {
      next(ApiError.serverError(err.message));
    }),

  releasePokemon: (req, res, next) => pokemonService.releasePokemon(req)
    .then((result) => {
      if (result.errors) return next(ApiError.badRequest(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => {
      next(ApiError.serverError(err.message));
    }),

};

module.exports = pokemonController;

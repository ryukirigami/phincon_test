const express = require('express');
const authentication = require('../middleware/authentication');

const api = express.Router();

const pokemonRoute = require('./pokemon/pokemonRoute');

api.use('/pokemon', authentication, pokemonRoute);

module.exports = api;

class Pokemon {
  uuid;

  pokemonId;

  pokemonName;

  nickname;

  currentSeqNum;

  lastSeqNum;

  constructor(uuid, pokemonId, pokemonName) {
    this.uuid = uuid;
    this.pokemonId = pokemonId;
    this.pokemonName = pokemonName;
  }
}

module.exports = Pokemon;

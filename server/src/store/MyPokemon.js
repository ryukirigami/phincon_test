class MyPokemon {
  pokemonList = [];

  addPokemon(pokemon) {
    this.pokemonList.push(pokemon);
  }

  getPokemons() {
    return this.pokemonList;
  }

  getPokemonByUUID(uuid) {
    return this.pokemonList.filter((pokemon) => pokemon.uuid === uuid);
  }

  getPokemonIndexByUUID(uuid) {
    return this.pokemonList.findIndex((pokemon) => pokemon.uuid === uuid);
  }

  updatePokemon(uuid, pokemon) {
    const index = this.getPokemonIndexByUUID(uuid);
    this.pokemonList[index] = pokemon;
  }

  removePokemon(uuid) {
    this.pokemonList = this.pokemonList.filter((pokemon) => pokemon.uuid !== uuid);
  }
}

module.exports = new MyPokemon();

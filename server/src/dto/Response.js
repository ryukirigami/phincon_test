class Response {
  constructor() {
    this.errors = undefined;
    this.data = undefined;
  }

  addError(error) {
    if (this.errors) {
      this.errors.push(error);
    } else {
      this.errors = [error];
    }
  }

  setErrors(errors) {
    this.errors = errors;
  }

  setData(data) {
    this.data = data;
  }
}

module.exports = Response;

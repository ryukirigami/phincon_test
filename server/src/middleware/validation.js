const ApiError = require('../dto/ApiError');

const validation = (schema) => async (req, res, next) => {
  try {
    await schema.validate({ ...req.body }, { abortEarly: false });
    return next();
  } catch (err) {
    return next(ApiError.badRequest(err.errors));
  }
};

module.exports = validation;

module.exports = {
  purge: [
    './dist/**/*.{js,css}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        pokeball: 'url(https://cdn.pixabay.com/photo/2019/11/27/14/06/pokemon-4657023_1280.png)',
      },
    },
  },
};

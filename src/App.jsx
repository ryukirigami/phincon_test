import React from 'react';
import './App.css';
import {
  BrowserRouter, Routes, Route,
} from 'react-router-dom';
import PokemonList from './pages/PokemonList';
import PokemonDetail from './pages/PokemonDetail';
import MyPokemonList from './pages/MyPokemonList';
import NoMatch from './pages/NoMatch';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import Pokeball from './components/Pokeball';

const App = () => {
  return (
    <BrowserRouter>
      <div className="w-full min-h-screen flex flex-col justify-between overflow-x-hidden">
        <Header />
        <div className="flex-1 bg-white">
          <Pokeball />
          <Routes>
            <Route path="/">
              <Route index element={<PokemonList />} />
              <Route
                path="my-pokemon"
                element={(
                  <React.Suspense fallback={<>...</>}>
                    <MyPokemonList />
                  </React.Suspense>
            )}
              />
              <Route
                path="/pokemon/:id"
                element={(
                  <React.Suspense fallback={<>...</>}>
                    <PokemonDetail />
                  </React.Suspense>
            )}
              />
              <Route path="*" element={<NoMatch />} />
            </Route>
          </Routes>
        </div>
        <Footer />
      </div>
    </BrowserRouter>
  );
};

export default App;

export const capitalizeFirstLetter = (word) => {
  return word.charAt(0).toUpperCase() + word.slice(1);
};

export const removeSeqSuffix = (word) => {
  const words = word.split('-');
  words.pop();
  return words.join('-');
};

export const getRandomColor = () => {
  const colors = ['red', 'green', 'purple', 'blue', 'gray', 'yellow', 'indigo', 'pink'];
  const random = Math.floor(Math.random() * colors.length);

  return colors[random];
};

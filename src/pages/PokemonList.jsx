import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { getPokemonList } from '../api/pokeApiService';

const PokemonList = () => {
  const [pokemons, setPokemons] = useState([]);

  useEffect(async () => {
    const res = await getPokemonList();
    setPokemons(res.data.results);
  }, []);

  return (
    <div className="bg-white">
      <div className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 className="text-2xl font-extrabold tracking-tight text-gray-900">PokemonList</h2>

        <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
          {pokemons.map((pokemon) => (
            <div key={pokemon.name} className="group relative">

              <Link to={`/pokemon/${pokemon.name}`}>
                <div className="w-full flex item-end min-h-80 bg-gray-50 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
                  <LazyLoadImage
                    className="w-full h-full object-center object-cover lg:w-full lg:h-full"
                    effect="blur"
                    alt={pokemon.name}
                    src={`https://img.pokemondb.net/artwork/large/${pokemon.name}.jpg`}
                  />
                </div>
              </Link>

              <div className="mt-4 flex justify-between">
                <div>
                  <h3 className="text-sm text-gray-700">
                    <Link to={`/pokemon/${pokemon.name}`}>
                      <span aria-hidden="true" className="absolute inset-0" />
                      {pokemon.name}
                    </Link>
                  </h3>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PokemonList;

import {
  useState, useEffect, Fragment,
} from 'react';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { pokemonCount } from '../redux/actions/pokemonAction';
import { getPokemonDetail } from '../api/pokeApiService';
import { capitalizeFirstLetter } from '../utils/common';
import { catchPokemon } from '../api/apiService';
import Modal from '../components/Modal';
import PokemonCaptureLoading from '../components/PokemonCaptureLoading';
import DialogMessage from '../components/DialogMessage';
import PokemonRenameForm from '../components/PokemonRenameForm';
import Alert from '../components/Alert';

const PokemonDetail = ({ pokeball, addPokeball }) => {
  const { id } = useParams();

  const [pokemon, setPokemon] = useState(null);

  const [modalContent, setModalContent] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [toastContent, setToastContent] = useState(null);
  const [isToastOpen, setIsToastOpen] = useState(false);

  useEffect(async () => {
    const res = await getPokemonDetail(id);
    setPokemon(res.data);

    return () => {
      setModalContent(null);
      setToastContent(null);
    };
  }, [id]);

  const capturePokemon = () => {
    setIsModalOpen(true);
    setModalContent(<PokemonCaptureLoading />);

    const data = {
      pokemonId: pokemon.id,
      pokemonName: pokemon.name,
    };

    setTimeout(async () => {
      setIsModalOpen(false);
      try {
        const res = await catchPokemon(data);

        addPokeball(pokeball + 1);

        setModalContent(<PokemonRenameForm setIsOpen={setIsModalOpen} pokemon={res.data.data.pokemon} setToast={setIsToastOpen} setToastContent={setToastContent}>{res.data.data.message}</PokemonRenameForm>);
      } catch (error) {
        setModalContent(<DialogMessage setIsOpen={setIsModalOpen} status="danger">{error.response.data.errors[0]}</DialogMessage>);
      } finally {
        setIsModalOpen(true);
      }
    }, 2000);
  };

  if (pokemon) {
    return (
      <div className="md:flex items-start justify-center py-12 2xl:px-20 md:px-6 px-4">
        <div className="xl:w-2/6 lg:w-2/5 md:w-1/2 w-full">
          <img className="w-full" alt="img of a girl posing" src={`https://img.pokemondb.net/artwork/large/${pokemon?.name}.jpg`} />
        </div>
        <div className="xl:w-2/5 md:w-1/2 lg:ml-8 md:ml-6 md:mt-0 mt-6">
          <div className="pb-6">
            <p className="text-sm leading-none text-gray-600">Pokemon Detail</p>
            <h1 className="lg:text-2xl text-xl font-semibold lg:leading-6 leading-7 text-gray-800 mt-2">
              {capitalizeFirstLetter(pokemon?.name)}
            </h1>
          </div>

          <div className="py-4 flex items-start justify-start">
            <p className="text-base leading-4 text-gray-800">Moves</p>
            <div className="flex ml-5 items-center justify-center">
              <div className="text-sm leading-none text-gray-600">
                {pokemon?.moves.map((item, index) => (
                  <Fragment key={item.move.name}>
                    <div className="bg-gray-500 text-white p-1 rounded-md inline-block mb-1">
                      {item.move.name}
                    </div>
                    {index === (pokemon.moves.length - 1) ? '' : ', '}
                  </Fragment>
                ))}
              </div>
            </div>
          </div>

          <div className="py-4 flex items-center justify-start">
            <p className="text-base leading-4 text-gray-800">Types</p>
            <div className="flex ml-6 items-center justify-center">
              <div className="text-sm leading-none text-gray-600">
                {pokemon?.types.map((item, index) => (
                  <Fragment key={item.type.name}>
                    <div className="bg-gray-500 text-white p-1 rounded-md inline-block mb-1">
                      {item.type.name}
                    </div>
                    {index === (pokemon.types.length - 1) ? '' : ', '}
                  </Fragment>
                ))}
              </div>
            </div>
          </div>

          <button
            onClick={capturePokemon}
            className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800text-base flex items-center justify-center leading-none text-white bg-gray-800 w-full py-4 mt-10 hover:bg-gray-700"
          >
            Catch
          </button>

          <Modal isOpen={isModalOpen} setIsOpen={setIsModalOpen}>{modalContent}</Modal>
        </div>
        <Alert flag={isToastOpen} setFlag={setIsToastOpen}>{toastContent}</Alert>
      </div>
    );
  }
  return (
    <div />
  );
};

const mapStateToProp = (globalState) => {
  return {
    pokeball: globalState.pokemon.myPokemonCount,
  };
};

const mapDispatchToProp = (dispatch) => {
  return {
    addPokeball: (data) => dispatch(pokemonCount(data)),
  };
};

export default connect(mapStateToProp, mapDispatchToProp)(PokemonDetail);

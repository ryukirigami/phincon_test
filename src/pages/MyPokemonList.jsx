import { useState, useEffect } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { connect } from 'react-redux';
import { pokemonCount } from '../redux/actions/pokemonAction';
import { getMyPokemonList, releasePokemon } from '../api/apiService';
import Alert from '../components/Alert';
import DialogMessage from '../components/DialogMessage';
import Modal from '../components/Modal';
import MyPokemonOptions from '../components/MyPokemonOptions';
import PokemonReleaseLoading from '../components/PokemonReleaseLoading';
import PokemonRenameForm from '../components/PokemonRenameForm';

const MyPokemonList = ({ pokeball, addPokeball, isReload }) => {
  const [pokemons, setPokemons] = useState([]);
  const [selectedPokemon, setSelectedPokemon] = useState(null);

  const [modalContent, setModalContent] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [toastContent, setToastContent] = useState(null);
  const [isToastOpen, setIsToastOpen] = useState(false);

  const fetchPokemons = async () => {
    const res = await getMyPokemonList();
    setPokemons(res.data.data.pokemonList);
  };

  useEffect(async () => {
    await fetchPokemons();

    return () => {
      setPokemons({});
    };
  }, []);

  useEffect(async () => {
    await fetchPokemons();
  }, [isReload]);

  const renamePokemonHandle = () => {
    setModalContent(<PokemonRenameForm setIsOpen={setIsModalOpen} pokemon={selectedPokemon} setToast={setIsToastOpen} setToastContent={setToastContent} />);
  };

  const releasePokemonHandle = async () => {
    setModalContent(<PokemonReleaseLoading />);
    setTimeout(async () => {
      try {
        const res = await releasePokemon(selectedPokemon.uuid);

        addPokeball(pokeball - 1);

        setModalContent(<DialogMessage setIsOpen={setIsModalOpen} status="success">{res.data.data.message}</DialogMessage>);
        await fetchPokemons();
      } catch (error) {
        setModalContent(<DialogMessage setIsOpen={setIsModalOpen} status="danger">{error.response.data.errors[0]}</DialogMessage>);
      } finally {
        setIsModalOpen(true);
      }
    }, 2000);
  };

  useEffect(async () => {
    setModalContent(<MyPokemonOptions pokemon={selectedPokemon} renamePokemonHandle={renamePokemonHandle} releasePokemonHandle={releasePokemonHandle} />);
  }, [selectedPokemon]);

  const showOptions = () => {
    setModalContent(<MyPokemonOptions pokemon={selectedPokemon} renamePokemonHandle={renamePokemonHandle} releasePokemonHandle={releasePokemonHandle} />);
    setIsModalOpen(true);
  };

  return (
    <>
      <div className="bg-white">
        <div className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
          <h2 className="text-2xl font-extrabold tracking-tight text-gray-900">My Pokemon List</h2>

          <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
            {pokemons.map((pokemon) => (
              <div key={pokemon.uuid} onClick={() => { setSelectedPokemon(pokemon); showOptions(); }} className="group relative cursor-pointer">
                <div className="w-full flex item-end min-h-80 bg-gray-50 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
                  <LazyLoadImage
                    className="w-full h-full object-center object-cover lg:w-full lg:h-full"
                    effect="blur"
                    alt={pokemon.pokemonName}
                    src={`https://img.pokemondb.net/artwork/large/${pokemon.pokemonName}.jpg`}
                  />
                </div>
                <div className="mt-4 flex justify-between">
                  <div>
                    <h3 className="text-sm text-gray-700">
                      {/* <Link to={`/pokemon/${pokemon.uuid}`}> */}
                      <span aria-hidden="true" className="absolute inset-0" />
                      {pokemon.nickname || pokemon.pokemonName}
                      {/* </Link> */}
                    </h3>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <Modal isOpen={isModalOpen} setIsOpen={setIsModalOpen}>{modalContent}</Modal>
      </div>
      <Alert flag={isToastOpen} setFlag={setIsToastOpen}>{toastContent}</Alert>
    </>
  );
};

const mapStateToProp = (globalState) => {
  return {
    pokeball: globalState.pokemon.myPokemonCount,
    isReload: globalState.pokemon.isReload,
  };
};

const mapDispatchToProp = (dispatch) => {
  return {
    addPokeball: (data) => dispatch(pokemonCount(data)),
  };
};

export default connect(mapStateToProp, mapDispatchToProp)(MyPokemonList);

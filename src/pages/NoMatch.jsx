import React from 'react';

const NoMatch = () => {
  return (
    <div className="flex w-screen justify-center mt-48">
      <h1 className="text-9xl text-gray-600">404.</h1>
    </div>
  );
};

export default NoMatch;

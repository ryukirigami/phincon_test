import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:8080/api/v1',
  headers: {
    'content-type': 'application/json',
    key: '1234567890',
  },
});

export const getMyPokemonList = () => {
  return api({
    method: 'GET',
    url: '/pokemon',
  });
};

export const catchPokemon = (pokemon) => {
  return api({
    method: 'POST',
    url: '/pokemon/catch',
    data: pokemon,
  });
};

export const renamePokemon = (id, nickname) => {
  return api({
    method: 'PUT',
    url: `/pokemon/rename/${id}`,
    data: nickname,
  });
};

export const releasePokemon = (id) => {
  return api({
    method: 'DELETE',
    url: `/pokemon/release/${id}`,
  });
};

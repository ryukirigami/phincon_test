import axios from 'axios';

const pokeApi = axios.create({
  baseURL: 'https://pokeapi.co/api/v2',
  headers: {
    'content-type': 'application/json',
  },
});

export const getPokemonList = () => {
  return pokeApi({
    method: 'GET',
    url: '/pokemon?limit=151',
  });
};

export const getPokemonDetail = (id) => {
  return pokeApi({
    method: 'GET',
    url: `/pokemon/${id}`,
  });
};

export const getPokemonImage = (id) => {
  return `https://pokeres.bastionbot.org/images/pokemon/${id}.png`;
};

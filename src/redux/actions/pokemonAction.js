import {
  POKEMON_COUNT, IS_RELOAD,
} from '../types';

export const pokemonCount = (content) => {
  return {
    type: POKEMON_COUNT,
    payload: content,
  };
};

export const reloadData = () => {
  return {
    type: IS_RELOAD,
  };
};

import {
  POKEMON_COUNT, IS_RELOAD,
} from '../types';

const initialState = {
  myPokemonCount: 0,
  isReload: false,
};

const pokemon = (state = initialState, action) => {
  switch (action.type) {
    case POKEMON_COUNT:
      const myPokemonCount = action.payload;
      return { ...state, myPokemonCount };

    case IS_RELOAD:
      return { ...state, isReload: !state.isReload };

    default:
      return state;
  }
};

export default pokemon;

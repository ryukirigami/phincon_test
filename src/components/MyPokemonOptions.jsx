import React from 'react';
import { Dialog } from '@headlessui/react';

const MyPokemonOptions = ({ pokemon, renamePokemonHandle, releasePokemonHandle }) => {
  return (
    <>
      <Dialog.Title
        as="h3"
        className="text-lg font-medium leading-6 text-gray-900"
      >
        {pokemon.nickname || pokemon.pokemonName}
      </Dialog.Title>
      <div className="mt-4 flex flex-col items-center">
        <button
          type="button"
          className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
          onClick={renamePokemonHandle}
        >
          Rename Pokemon
        </button>

        <button
          type="button"
          className="inline-flex mt-5 justify-center px-4 py-2 text-sm font-medium text-red-900 bg-red-100 border border-transparent rounded-md hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-red-500"
          onClick={releasePokemonHandle}
        >
          Release Pokemon
        </button>
      </div>
    </>
  );
};

export default MyPokemonOptions;

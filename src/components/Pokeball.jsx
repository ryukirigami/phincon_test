import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Pokeball = ({ pokeball }) => {
  return (
    <Link to="/my-pokemon">
      <div
        className="flex justify-center items-center w-20 h-20 bg-pokeball bg-cover rounded-lg fixed bottom-5 right-5 cursor-pointer transition-all ease-in-out z-50 opacity-50 hover:opacity-95"
      >
        <div
          style={{ textShadow: '0px 0px 10px white' }}
          className="text-4xl text-gray-900 font-bold opacity-100  rounded-md px-2"
        >
          {pokeball}

        </div>
      </div>
    </Link>
  );
};

const mapStateToProp = (globalState) => {
  return {
    pokeball: globalState.pokemon.myPokemonCount,
  };
};

export default connect(mapStateToProp)(Pokeball);

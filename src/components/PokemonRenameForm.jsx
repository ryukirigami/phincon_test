import React, { useState } from 'react';
import { Dialog } from '@headlessui/react';
import { CheckIcon } from '@heroicons/react/outline';
import { connect } from 'react-redux';
import { reloadData } from '../redux/actions/pokemonAction';
import { removeSeqSuffix, capitalizeFirstLetter } from '../utils/common';
import { renamePokemon } from '../api/apiService';

function PokemonRenameForm({
  children, setIsOpen, pokemon, setToast, setToastContent, reload,
}) {
  const [nickname, setNickname] = useState(pokemon.nickname ? removeSeqSuffix(pokemon.nickname) : capitalizeFirstLetter(pokemon.pokemonName));

  const closeModal = () => {
    setIsOpen(false);
  };

  const onSubmit = async (e) => {
    e.preventDefault();

    try {
      const res = await renamePokemon(pokemon.uuid, { nickname });

      setToastContent(res.data.data.message);
      setToast(true);
      reload();
    } catch (error) {
      setToastContent(error.response.data.errors[0]);
      setToast(true);
    } finally {
      closeModal();
    }
  };

  return (
    <form onSubmit={onSubmit} method="POST">

      <div className="sm:flex sm:items-start">
        <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
          <CheckIcon className="h-6 w-6 text-green-600" aria-hidden="true" />
        </div>

        <div className="mt-3 w-full text-center sm:mt-0 sm:ml-4 sm:text-left">
          <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
            Rename Pokemon
          </Dialog.Title>
          <div className="mt-2">
            <p className="text-sm text-gray-500">
              {children}
            </p>
            <div className="mt-8">
              <input
                type="text"
                name="nickname"
                value={nickname}
                onChange={(e) => { setNickname(e.target.value); }}
                required
                className="mt-1 focus:ring-green-500 p-2 block w-full shadow-sm text-md border-2 border-gray-300 rounded-md"
              />
            </div>
          </div>
        </div>
      </div>

      <div className="mt-10 flex justify-end">
        <button
          type="submit"
          className="inline-flex justify-center px-4 py-2 text-sm font-medium text-green-900 bg-green-100 border border-transparent rounded-md hover:bg-green-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-green-500"
        >
          Save
        </button>
      </div>
    </form>

  );
}

const mapStateToProp = () => {
  return {};
};

const mapDispatchToProp = (dispatch) => {
  return {
    reload: () => dispatch(reloadData()),
  };
};

export default connect(mapStateToProp, mapDispatchToProp)(PokemonRenameForm);

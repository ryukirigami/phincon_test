import React from 'react';
import psyduckImage from '../images/pokemon-release.gif';

const PokemonReleaseLoading = () => {
  return (
    <div className="w-full flex justify-center">
      <img src={psyduckImage} alt="psyduck" />
    </div>
  );
};

export default PokemonReleaseLoading;

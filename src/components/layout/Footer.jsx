// import Link from 'next/link';
const Footer = () => {
  return (
    <div>
      <footer id="footer" className="relative bg-gray-900 pt-5">
        <div className="py-6 flex flex-col justify-center items-center">
          <p className="text-xs lg:text-sm leading-none text-gray-50">
            2021 © Deni Triyono.
          </p>
        </div>
      </footer>
    </div>
  );
};
export default Footer;

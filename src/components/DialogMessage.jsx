import React, { useEffect, useState } from 'react';
import { Dialog } from '@headlessui/react';
import {
  ExclamationIcon,
  CheckIcon,
} from '@heroicons/react/outline';

const dialogThemes = {
  danger: {
    color: 'red',
    icon: ExclamationIcon,
  },
  warning: {
    color: 'yellow',
    icon: ExclamationIcon,
  },
  success: {
    color: 'green',
    icon: CheckIcon,
  },
};

const DialogMessage = ({ children, setIsOpen, status }) => {
  const [dialogStatus, setDialogStatus] = useState(dialogThemes.success);

  useEffect(() => {
    setDialogStatus(dialogThemes[status]);
  }, []);

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <>
      <div className="sm:flex sm:items-start">
        <div className={`mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-${dialogStatus.color}-100 sm:mx-0 sm:h-10 sm:w-10`}>
          <dialogStatus.icon className={`h-6 w-6 text-${dialogStatus.color}-600`} aria-hidden="true" />
        </div>

        <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
          <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
            Message
          </Dialog.Title>
          <div className="mt-2">
            <p className="text-sm text-gray-500">
              {children}
            </p>
          </div>
        </div>
      </div>

      <div className="mt-4 flex justify-end">
        <button
          type="button"
          className={`inline-flex justify-center px-4 py-2 text-sm font-medium text-${dialogStatus.color}-900 bg-${dialogStatus.color}-100 border border-transparent rounded-md hover:bg-${dialogStatus.color}-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500`}
          onClick={closeModal}
        >
          Close
        </button>
      </div>
    </>
  );
};

export default DialogMessage;

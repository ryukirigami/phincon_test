import React from 'react';
import pokeBallImage from '../images/pokeball.gif';

const PokemonCaptureLoading = () => {
  return (
    <img src={pokeBallImage} alt="poke-ball" />
  );
};

export default PokemonCaptureLoading;
